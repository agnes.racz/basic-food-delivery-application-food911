package org.fasttrackit;

public class Reviews {
    private final String author;
    private final String review;
    private final double rating;


    public Reviews(String author, String review, double rating) {
        this.author = author;
        this.review = review;
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public String getReview() {
        return review;
    }

    public String getAuthor() {
        return author;
    }
}
