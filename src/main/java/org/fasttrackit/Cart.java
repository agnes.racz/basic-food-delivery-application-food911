package org.fasttrackit;

public class Cart {

    private final String itemName;

    private final int itemPrice;

    public Cart(String itemName, int itemPrice) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemPrice() {
        return itemPrice;
    }
}
