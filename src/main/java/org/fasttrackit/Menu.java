package org.fasttrackit;

public class Menu {

    private final String foodName;
    private final int foodPrice;

    public Menu(String foodName, int foodPrice) {

        this.foodName = foodName;
        this.foodPrice = foodPrice;
    }

    public String getFoodName() {
        return foodName;
    }

    public int getFoodPrice() {
        return foodPrice;
    }

}
