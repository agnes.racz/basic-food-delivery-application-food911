package org.fasttrackit;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    // Used colors for printing
    public static final String MAGENTA = "\u001B[35m";
    public static final String MAGENTABACK = "\u001B[45m";
    public static final String RED = "\u001B[31m";
    public static final String GREEN = "\u001B[32m";
    public static final String RESET = "\u001B[0m";

    // Global variables
    private static Scanner scanner;
    private static int deliveryOrPickUp;
    private static final List<Restaurant> restaurants = new ArrayList<>();
    private static final List<Cart> cartContent = new ArrayList<>(); // A list containing the added items to cart

    public static void main(String[] args) {
        buildRestaurants(); // Register the restaurants, their menus and reviews
        printWelcome(); // Print the welcome text

        int selection = 0; // A variable given by the user in console, when choosing the action wanted from the printed menu

        while (selection != 6) {

            printMenu();  // Print the menu
            selection = askUserToSelectNumber(); // User gives the wanted action's number

            switch (selection) {
                case 1 -> orderFood();

                case 2 -> printReviews();

                case 3 -> printRatings();

                case 4 -> leaveReview();

                case 5 -> showBestRestaurant();

                case 6 -> exit();

                default -> printInvalidInput();
            }
        }
    }

    private static void exit() {
        System.out.println("\n" + MAGENTA + "Thank you for using Food911! You can always count on us in case of Foodmergency!" + RESET);
    }

    private static void showBestRestaurant() {
        double maxRating = restaurants.get(0).calculateRating(); // maxRating will start with the first restaurant average rating
        String bestRestaurant = restaurants.get(0).getName();

        for (int i = 1; i < restaurants.size(); i++) {
            if (restaurants.get(i).calculateRating() > maxRating) {

                maxRating = restaurants.get(i).calculateRating(); // If another restaurant's rating is higher than maxRating, maxRating becomes the actual restaurant's rating
                bestRestaurant = restaurants.get(i).getName();
            }
        }

        System.out.println("\nThe best restaurant in town is: " + MAGENTABACK + bestRestaurant + RESET + ", rating: " + maxRating);
        printWhatsNext();
    }

    private static int askForRating() {
        int customerRating = 0;
        while ((customerRating < 1) || (customerRating > 5)) { // Valid customer rating is 1-5
            System.out.print("Please enter rating between 1-5: ");
            customerRating = scanner.nextInt();
        }
        return customerRating;
    }

    private static String askForReviewText() {
        System.out.print("Please enter your review: ");
        return scanner.nextLine();
    }

    private static String askForName() {
        System.out.print("Please enter your name: ");

        scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static void askForReview(int s) {
        String customerName = askForName(); // Ask user's name

        String customerReview = askForReviewText(); // Ask user's text review

        int customerRating = askForRating(); // Ask user's rating

        restaurants.get(s).addReview(customerName, customerReview, customerRating); // Add review to the existing reviews
        System.out.println(GREEN + "Your review was added successfully!" + RESET);
    }


    private static void leaveReview() {

        showRestaurants();
        int s = askUserToSelectNumber();
        s = repeatUntilCorrectRestaurant(s) - 1;

        showSelectedRestaurant(s);
        askForReview(s); // Ask user for review
        System.out.println(restaurants.get(s).getReviews()); // Print all reviews

        printWhatsNext();
    }

    private static void printRestaurantsAndRatings() {
        DecimalFormat df = new DecimalFormat("#.##");

        System.out.println("\n\t\t" + MAGENTA + "Here are the restaurants and their ratings: " + RESET);

        for (int i = 0; i < restaurants.size(); i++) {
            System.out.print("\n\t\t" + (i + 1) + ") " + restaurants.get(i).getName() + " " + df.format(restaurants.get(i).calculateRating()));
        }
    }

    private static void printRatings() {

        printRestaurantsAndRatings();

        System.out.println();
        printWhatsNext();
    }

    private static void printRestaurantRating(Restaurant selectedRestaurant) {

        DecimalFormat df = new DecimalFormat("#.##");
        double restaurantRating = selectedRestaurant.calculateRating(); // Calculate average rating of the restaurant

        System.out.println("\n\t\t\tRating: " + df.format(restaurantRating));
    }

    private static void printReviews() {
        showRestaurants(); // Print restaurants
        int s = askUserToSelectNumber();
        s = repeatUntilCorrectRestaurant(s) - 1; // -1, because the printed list numbering is higher with 1, than the ArrayList

        showSelectedRestaurant(s); // Print the restaurant selected by the user

        Restaurant selectedRestaurant = restaurants.get(s);

        printRestaurantRating(selectedRestaurant); // Print the selected restaurant's rating

        System.out.println(selectedRestaurant.getReviews()); // Print the reviews of the selected restaurant

        printWhatsNext(); // Ask user what is the next action?
    }

    private static void printWhatsNext() {
        System.out.println("\n\t\t" + MAGENTA + "What would you like to do next?" + RESET);
    }

    private static void printThankYou() {

        if (deliveryOrPickUp == 1) {
            System.out.println("\nThank you for your order! It will be ready for Pick Up in 25 minutes!");
        } else {
            System.out.println("Thank you for your order! It will be delivered in 60 minutes!");
        }
    }

    private static int calculateTotalPrice() {
        int total = 0;

        for (Cart cart : cartContent) {
            total += cart.getItemPrice();
        }

        return total;
    }

    private static void printTotal() {
        System.out.println("\n\t\t" + GREEN + "The total is: " + calculateTotalPrice() + " RON" + RESET);
    }

    private static void printCart() {
        System.out.println("\n\t\t" + MAGENTA + "Your cart:" + RESET + "\n");

        for (Cart cart : cartContent) {
            System.out.println("\t\t" + cart.getItemName() + "  " + cart.getItemPrice() + " RON");
        }
    }

    private static int repeatUntilCorrectFood(Restaurant selectedRestaurant, int selectFood) {

        while (selectFood > selectedRestaurant.findNumber()) {
            printInvalidInput();
            if (cartContent.isEmpty()) {
                System.out.println(selectedRestaurant.getMenu());
            } else {
                System.out.println(selectedRestaurant.getMenu());
                System.out.println("\t\t" + GREEN + "10) Check Out" + RESET);
            }

            selectFood = askUserToSelectNumber();
        }
        return selectFood;
    }

    private static void addToCart(Restaurant selectedRestaurant) {
        int selectedFood;

        do {
            if (cartContent.isEmpty()) {  // If nothing was selected yet, print the restaurants menu
                System.out.println(selectedRestaurant.getMenu());
            } else {
                System.out.println(selectedRestaurant.getMenu());
                System.out.println("\t\t" + GREEN + "10) Check Out" + RESET); // If cart already has 1 item, print rest. menu with the extra option for Check out
            }

            selectedFood = askUserToSelectNumber();

            if (selectedFood == 10) {
                break;
            } else {
                selectedFood = repeatUntilCorrectFood(selectedRestaurant, selectedFood) - 1; // Repeat until user chooses a valid number
                // selectedFood becomes selectedFood-1 because, the printed Food list numbering is higher with 1, than the Menu Arraylist
                String selectedFoodName = selectedRestaurant.findFoodName(selectedFood); // Find the selected food name
                int selectedFoodPrice = selectedRestaurant.findFoodPrice(selectedFood); // Find the selected food price

                cartContent.add(new Cart(selectedFoodName, selectedFoodPrice)); // Add to cart
                System.out.println("\t\t" + GREEN + selectedFoodName + " added successfully!" + RESET);
            }
        }
        while (selectedFood != 10); // Repeat until user finishes the order

        if (deliveryOrPickUp == 2) {
            cartContent.add(new Cart("Delivery", 10)); // Add delivery to order in case user chose "delivery"
        }
    }

    private static int repeatUntilCorrectDelivery(int deliveryOrPickUp, int max, int s) {
        while (deliveryOrPickUp > max) {

            printInvalidInput();
            showDeliveryOptions(s);

            deliveryOrPickUp = askUserToSelectNumber();
        }
        return deliveryOrPickUp;
    }

    private static boolean showDeliveryOptions(int selectedRestaurant) {

        if (restaurants.get(selectedRestaurant).getDelivery()) {
            System.out.println("\t\t1) Pick Up in 25 minutes - 0 RON\n\t\t2) Delivery in 60 minutes - 10 RON");
        } else {
            System.out.println("\t\t" + RED + "ATTENTION!!! The selected restaurant does not support DELIVERY!!!" + RESET);
            System.out.println("\t\t1) Pick Up in 25 minutes - 0 RON");
        }
        return restaurants.get(selectedRestaurant).getDelivery();
    }

    private static void showSelectedRestaurant(int selectedRestaurant) {

        System.out.println("\n\t\t" + MAGENTA + "You selected " + RESET + MAGENTABACK + restaurants.get(selectedRestaurant).getName() + RESET + MAGENTA + " restaurant" + RESET);
    }

    private static void printInvalidInput() {
        System.out.println(RED + "Invalid input! Please try again!" + RESET + "\n");
    }

    private static int repeatUntilCorrectRestaurant(int s) {

        while (s > 4) {
            printInvalidInput();
            showRestaurants();

            s = askUserToSelectNumber();
        }
        return s;
    }

    private static void showRestaurants() {

        System.out.println("\n\t\t" + MAGENTA + "Here is a list of available restaurants: " + RESET);

        for (int i = 0; i < restaurants.size(); i++) {
            System.out.print("\t\t" + (i + 1) + ") " + restaurants.get(i).getName());
            System.out.println("  ***  " + restaurants.get(i).getDescription());
        }
    }

    private static void orderFood() {
        showRestaurants();  // Print the restaurants and their description

        int s = askUserToSelectNumber();
        s = repeatUntilCorrectRestaurant(s) - 1; // Repeat the question until the user gives a valid number
        // s becomes s-1, because the restaurant list numbering on the console is higher with 1, than the restaurant Arraylist

        showSelectedRestaurant(s); // Print the selected restaurant
        int maxOptions; // maxOptions is the number of options of delivery for a specific restaurant

        if (showDeliveryOptions(s)) { // Find out the delivery options
            maxOptions = 2;
        } else maxOptions = 1;

        deliveryOrPickUp = askUserToSelectNumber(); // User gives the wanted delivery option
        deliveryOrPickUp = repeatUntilCorrectDelivery(deliveryOrPickUp, maxOptions, s); // Repeat the question until the user gives a valid number

        Restaurant selectedRestaurant = restaurants.get(s);  // selectedRestaurant becomes the restaurant selected by the user

        addToCart(selectedRestaurant); // Adding food items to cart

        printCart();  // Print cart content (food name + price)
        printTotal(); // Calculate total
        printThankYou(); // Print Thank You message

        cartContent.clear(); // Empty Cart
        printWhatsNext(); // Ask user about next move
    }

    private static int askUserToSelectNumber() {

        scanner = new Scanner(System.in);

        System.out.print("\nPlease select a number from above: ");
        return scanner.nextInt();
    }

    private static void printMenu() {

        System.out.println("\t\t1) I would like to order food\n\t\t2) I would like to read the reviews of a restaurant\n\t\t3) I would like to see the ratings of each restaurant\n\t\t4) I would like to leave a review\n\t\t5) I would like to see the best restaurant\n\t\t6) Exit");
    }

    private static void printWelcome() {

        System.out.println("\n\t\t" + MAGENTA + "Hello! What is your Foodmergency?" + RESET);
    }

    private static void buildRestaurants() {

        Restaurant laDolceVita = new Restaurant("La Dolce Vita", "Pasta and Pizza");
        laDolceVita.setDelivery(true);
        laDolceVita.addFood("Pizza ", 30);
        laDolceVita.addFood("Pasta", 40);
        laDolceVita.addFood("Panini", 50);
        laDolceVita.addReview("Paul", "Excellent food", 5);
        laDolceVita.addReview("Rebecca", "Good pizza", 4);
        laDolceVita.addReview("Maria", "Music too loud", 1);

        Restaurant tidesBeachClub = new Restaurant("Tides Beach Club", "Fish and Shellfish");
        tidesBeachClub.setDelivery(false);
        tidesBeachClub.addFood("Calamari", 80);
        tidesBeachClub.addFood("Shrimp", 100);
        tidesBeachClub.addFood("Lobster", 150);
        tidesBeachClub.addReview("Sharon", "Delicious Fried Calamari", 4);
        tidesBeachClub.addReview("Cristina", "Sparkling clean", 5);

        Restaurant chopHouse = new Restaurant("Chop House", "Steaks and Chops");
        chopHouse.setDelivery(false);
        chopHouse.addFood("Sirloin", 120);
        chopHouse.addFood("Ribs", 80);
        chopHouse.addFood("Pork Chop", 100);
        chopHouse.addFood("Filet Mignon", 150);
        chopHouse.addReview("Philip", "Filet Mignon overcooked", 2);

        Restaurant bingoBurger = new Restaurant("Bingo Burger", "Burgers and Shakes");
        bingoBurger.setDelivery(true);
        bingoBurger.addFood("Beef Burger", 50);
        bingoBurger.addFood("Turkey Burger", 50);
        bingoBurger.addFood("Chicken Burger", 50);
        bingoBurger.addFood("Veggie Burger", 50);
        bingoBurger.addFood("Milkshake", 30);
        bingoBurger.addReview("Anne", "Milkshake too cold", 1);
        bingoBurger.addReview("Tade", "Chicken Burger undercooked", 2);
        bingoBurger.addReview("Laura", "Best restaurant ever!", 5);

        restaurants.add(laDolceVita);
        restaurants.add(tidesBeachClub);
        restaurants.add(chopHouse);
        restaurants.add(bingoBurger);
    }
}
