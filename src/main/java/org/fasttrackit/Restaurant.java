package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    public static final String MAGENTA = "\u001B[35m";
    public static final String RESET = "\u001B[0m";

    private final String name;

    private final String description;

    private boolean delivery;

    private final List<Menu> menuItems = new ArrayList<>();

    private final List<Reviews> reviews = new ArrayList<>();

    public Restaurant(String name, String description) {
        this.name = name;
        this.description = description;
        this.delivery = false;
    }

    public String findFoodName(int selectFood) {
        return menuItems.get(selectFood).getFoodName();
    }

    public int findFoodPrice(int selectFood) {
        return menuItems.get(selectFood).getFoodPrice();
    }

    public void addReview(String author, String text, int rating) {
        reviews.add(new Reviews(author, text, rating));
    }

    public String getReviews() {
        String review = "\n\t\t\tReviews:";

        for (Reviews value : reviews) {
            review += "\n\t\t" + value.getAuthor() + ": \"" + value.getReview() + "\", " + value.getRating();
        }
        return review;
    }

    public double calculateRating() {
        double restaurantRating = 0;
        int i;

        for (i = 0; i < reviews.size(); i++) {
            double d = reviews.get(i).getRating();
            restaurantRating += d;
        }
        return restaurantRating / i;
    }


    public void addFood(String foodName, int foodPrice) {
        menuItems.add(new Menu(foodName, foodPrice));
    }

    public String getMenu() {
        String menu = "\n" + MAGENTA + "\t\t" + name + RESET + " Menu:";
        int i = 1;
        for (Menu menuItem : menuItems) {
            menu += "\n\t\t" + i + ") " + menuItem.getFoodName() + " " + menuItem.getFoodPrice() + " RON";
            i++;
        }
        return menu;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDelivery(boolean deliveryOption) {
        this.delivery = deliveryOption;
    }

    public boolean getDelivery() {
        return delivery;
    }

    public int findNumber() {
        return menuItems.size();
    }
}


